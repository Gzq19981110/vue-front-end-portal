import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)
// 主页菜单
export const MainMenu = [
  {
    path: '/home',
    title: '主页',
    name: 'home',
    component: () => import('@/views/Home.vue')
  },
  {
    path: '/product',
    name: 'product',
    title: '产品',
    component: () => import('@/views/product/index.vue'),
    children: [
      {
        path: 'list',
        component: () => import('@/views/product/List.vue'),
        name: 'productList'
      },
      {
        path: 'edit',
        component: () => import('@/views/product/Edit.vue'),
        name: 'productEdit'
      }
    ]
  },
  {
    path: '/about',
    title: '关于我们',
    name: 'about',
    component: () => import('@/views/About.vue')
  }
]
// 隐藏的菜单
export const menuList = [
  {
    path: '/',
    name: 'layout',
    redirect: '/home',
    component: () => import('@/views/Layout.vue'),
    children: MainMenu
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/Login.vue')
  },
  {
    path: '/register',
    name: 'register',
    component: () => import('@/views/Register.vue')
  },
  {
    path: '*',
    redirect: '/home'
  }
]
console.log('this========>?', MainMenu)

const router = new VueRouter({
  routes: menuList,
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return {
        x: 0,
        y: 0
      }
    }
  }
})

/**
 * 重写路由的push方法， 解决重复点击菜单控制台异常问题
 */
const routerPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return routerPush.call(this, location).catch((error) => error)
}

export default router
