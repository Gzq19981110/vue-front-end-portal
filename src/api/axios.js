import axios from 'axios'
import { baseURL } from './copy'
import { Message } from 'element-ui'
// import { getToken } from "@/utils/auth";
class HttpRequest {
  constructor() {
    this.baseURL = baseURL
    this.queue = {}
  }
  getInsideConfig() {
    const config = {
      baseURL: this.baseURL,
      withCredentials: true, // send cookies when cross-domain requests
      timeout: 100000, // request timeout
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json;charset=utf-8'
        // Token: getToken()
      }
    }
    return config
  }
  interceptors(instance) {
    instance.interceptors.request.use(
      config => {
        // load = Loading.service({
        //   lock: true,
        //   customClass: "create-isLoading",
        //   text: "拼命加载中",
        //   spinner: "el-icon-loading",
        //   background: "rgba(0, 0, 0, 0.7)"
        // });
        return config
      },
      error => {
        return Promise.reject(error)
      }
    )
    instance.interceptors.response.use(
      response => {
        // load.close();
        if (response.data.code === 9001) {
          Message({
            message: response.data.msg,
            type: 'error'
          })
        } else if (response.data.code !== '200') {
          Message({
            message: response.data.msg,
            type: 'error'
          })
          return Promise.reject(response.data.msg)
        } else {
          return response.data
        }
      },
      error => {
        // load.close();
        Message({
          message: error.response.data.msg,
          type: 'error'
        })
        return Promise.reject(error)
      }
    )
  }
  request(options) {
    const instance = axios.create()
    var potions = Object.assign(this.getInsideConfig(), options)

    this.interceptors(instance)
    return instance(potions)
  }
}
export default HttpRequest
