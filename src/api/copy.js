export const baseURL =
  process.env.NODE_ENV === 'production' ?
    window._CONFIG['baseUrl'] : // 生产环境下请求url的地址
    'test-api' // 开发环境
