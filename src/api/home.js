import axios from './index'
// http://10.10.3.158:10002/leagueMisPro
export function query(data) {
  return axios.request({
    url: '/leagueMisPro/actions/bigScreen/information/query',
    method: 'get',
    params: data
  })
}
